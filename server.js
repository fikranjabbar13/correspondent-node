const express = require("express");
const bodyParser = require("body-parser");

const app = express();
const port = process.env.PORT || 3001;

app.use(bodyParser.json());

app.post("/correspondent", (req, res) => {
  const { fullname, birthdate } = req.body;

  // Proses data dan kalkulasi umur
  const nameInitial = fullname.charAt(0).toUpperCase();
  const birthYear = new Date(birthdate).getFullYear();
  const currentYear = new Date().getFullYear();
  const age = currentYear - birthYear;

  res.json({ initial: nameInitial, age });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
